<?php

namespace simpleHandle\Exception;

use Exception;

class UtilException extends Exception
{
    const EasyPdo_ERROR_CODE   = 111; // 数据库PDO链接错误
    const EasyRedis_ERROR_CODE = 222; // redis链接错误
    const EasyMQ_ERROR_CODE    = 333; // MQ链接错误
    const Logger_ERROR_CODE    = 444; // 日志错误
    const UtilTool_ERROR_CODE  = 555; // 工具集错误
    const EastXls_ERROR_CODE   = 666; // Excel操作错误
    const Consul_ERROR_CODE    = 777; // Consul操作错误
}