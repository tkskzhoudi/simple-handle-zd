<?php

declare(strict_types=1);

namespace simpleHandle\Component\UtilPdo;

class Raw
{
    public array  $map;
    public string $value;
}
