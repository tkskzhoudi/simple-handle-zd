<?php

namespace simpleHandle\Component\UtilTool;

use Godruoyi\Snowflake\Snowflake;

class SnowflakeInstance
{
    protected static ?Snowflake $_instance = null;
    public static function getInstance(int $dataCenterId,int $workerId): Snowflake
    {
        if (self::$_instance === null) {
            self::$_instance = new Snowflake($dataCenterId, $workerId);
        }
        return self::$_instance;
    }
}