<?php

namespace simpleHandle\Component\UtilRpcServer;

use Swoole\Process;
use Swoole\Server;

class RpcServer
{
    public Server $server;

    public function __construct(string $RpcHost, int $RpcPort)
    {
        $this->server = new Server($RpcHost, $RpcPort);
    }

    public function SetWorkerNum($workerNum): RpcServer
    {
        $this->server->set(['worker_num' => $workerNum]);
        return $this;
    }

    public function SetMaxRequest($MaxRequest): RpcServer
    {
        $this->server->set(['max_request' => $MaxRequest]);
        return $this;
    }

    public function SetMaxConn($MaxConn): RpcServer
    {
        $this->server->set(['max_conn' => $MaxConn]);
        return $this;
    }

    public function AddProcess(Process $process): RpcServer
    {
        $this->server->addProcess($process);
        return $this;
    }

    /*
     * $server, $fd
     */
    public function Connect($callback): RpcServer
    {
        $this->server->on('connect', $callback);
        return $this;
    }

    /*
     * $server, $fd, $reactor_id, $data
     */
    public function Receive($callback): RpcServer
    {
        $this->server->on('receive', $callback);
        return $this;
    }

    /*
     * $server, $fd
     */
    public function Close($callback): RpcServer
    {
        $this->server->on('close', $callback);
        return $this;
    }

    public function Start()
    {
        $this->server->start();
    }
}