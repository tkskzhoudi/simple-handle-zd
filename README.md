# simple-handle/util

### 介绍

* redis和mysql的pdo链接基于`swoole`协程下使用
* UtilXls excel表格操作基于`Xlswriter` 扩展
* 其他插件可在任何环境中使用

### 软件架构

`swoole` `Xlswriter` `pdo`

### 安装教程

`composer require simple-handle/util`

### 使用说明

#### RabbitMQ

参考文档  [php-amqplib](http://php-amqplib.github.io/php-amqplib/)

继承 simpleHandle\Component\UtilMQ\RabbitMQ 类

```php
use simpleHandle\Component\UtilMQ\RabbitMQ;
class MQ extends RabbitMQ
{
    /**
    * @throws Exception
    */
    public function __construct() {
        $mqConfig = [
            "host"         => "127.0.0.1",
            "port"         => 5672,
            "username"     => "guest",
            "password"     => "guest",
            "vhost"        => "/",
            "exchangeName" => "exchangeName",
            "queuePrefix"  => "queuePrefix"
	    ];
        parent::__construct( $mqConfig );
    }
}

$MQ = new MQ();
```

| 属性方法        | 类型                 | 描述                                 |
| --------------- | -------------------- | ------------------------------------ |
| MQConn          | AMQPStreamConnection | MQ链接                               |
| MQChan          | AMQPChannel          | MQ通道                               |
| messageId       | string               | MQ 发送消息的消息ID                  |
| exchangeName    | string               | 交换机名                             |
| queuePrefix     | string               | 队列名前缀                           |
| exchangeDeclare | function             | 创建Exchange交换机                   |
| queueDeclare    | function             | 创建Queue队列                        |
| queueBind       | function             | 队列绑定                             |
| publish         | function             | 发送队列消息（生产者）               |
| consumer        | function             | 监听队列消息（推模式）（消费者）     |
| send            | function             | 发送队列消息封装（生产者）           |
| listen          | function             | 监听队列消息封装（推模式）（消费者） |
| close           | function             | 关闭MQConn 关闭MQChan                |

##### 配置

| 配置项       | 必填 | 配置值       | 类型   |
| ------------ | ---- | ------------ | ------ |
| host         | ✅    | 127.0.0.1    | string |
| port         | ✅    | 5672         | int    |
| username     | ✅    | guest        | string |
| password     | ✅    | guest        | string |
| vhost        | ✅    | /            | string |
| exchangeName |      | exchangeName | string |
| queuePrefix  |      | queueName    | string |

##### MQConn

获取MQ链接

```php
$MQ->MQConn
```

##### MQChan

获取MQ通道Channel

```php
$MQ->MQChan
```

##### messageId

获取消息ID

##### exchangeDeclare

创建Exchange交换机

```php
// @throws UtilException
$MQ->exchangeDeclare( string $exchangeName, string $type, bool $durable, bool $autoDelete, array $arguments = array() )
```

###### 参数

| 参数          | 类型                                                         | 描述                                                         |
| ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $exchangeName | string                                                       | 交换机名称                                                   |
| $type         | string                                                       | [direct](http://php-amqplib.github.io/php-amqplib/classes/PhpAmqpLib-Exchange-AMQPExchangeType.html#constant_DIRECT)  \|  [fanout](http://php-amqplib.github.io/php-amqplib/classes/PhpAmqpLib-Exchange-AMQPExchangeType.html#constant_FANOUT) \| [headers](http://php-amqplib.github.io/php-amqplib/classes/PhpAmqpLib-Exchange-AMQPExchangeType.html#constant_HEADERS) \| [topic](http://php-amqplib.github.io/php-amqplib/classes/PhpAmqpLib-Exchange-AMQPExchangeType.html#constant_TOPIC) |
| $durable      | bool                                                         | 是否持久化                                                   |
| $autoDelete   | bool                                                         | 是否自动删除                                                 |
| $arguments    | [array](http://php-amqplib.github.io/php-amqplib/classes/PhpAmqpLib-Wire-AMQPTable.html) | 参数                                                         |

##### queueDeclare

创建Queue队列

```php
// @throws UtilException
$MQ->queueDeclare( string $queueName, bool $durable, bool $autoDelete, $arguments = array() )
```

###### 参数

| 参数        | 类型                                                         | 描述         |
| ----------- | ------------------------------------------------------------ | ------------ |
| $queueName  | string                                                       | 队列名称     |
| $durable    | bool                                                         | 是否持久化   |
| $autoDelete | bool                                                         | 是否自动删除 |
| $arguments  | [array](http://php-amqplib.github.io/php-amqplib/classes/PhpAmqpLib-Wire-AMQPTable.html) | 参数         |

##### queueBind

绑定队列

```php
// @throws UtilException
$MQ->queueBind( string $queueName, string $exchangeName, string $routingKey, array $arguments = array() )
```

###### 参数

| 参数          | 类型                                                         | 描述       |
| ------------- | ------------------------------------------------------------ | ---------- |
| $queueName    | string                                                       | 队列名称   |
| $exchangeName | string                                                       | 交换机名称 |
| $routingKey   | string                                                       | 路由键     |
| $arguments    | [array](http://php-amqplib.github.io/php-amqplib/classes/PhpAmqpLib-Wire-AMQPTable.html) | 参数       |

##### publish

发送MQ消息

```php
// @throws UtilException
$MQ->publish( AMQPMessage $msg, string $exchangeName, string $routingKey )
```

###### 参数

| 参数          | 类型        | 描述       |
| ------------- | ----------- | ---------- |
| $msg          | AMQPMessage | MQ消息     |
| $exchangeName | string      | 交换机名称 |
| $routingKey   | string      | 路由键     |

##### consumer

```php
// @throws UtilException
$callback = function(AMQPMessage $msg){
  // 处理消息逻辑......
  // ack
  $msg->getChannel()->basic_ack( $msg->getDeliveryTag() );
}
$MQ->consumer(
		string   $queueName,
		string   $tag,
		bool     $noAck,
		callable $callback,
		array    $arguments = array(),
		int      $prefetchSize = null,
		int      $prefetchCount = 1,
		bool     $nonBlocking = false,
		int      $timeOut = 0
	)
```

###### 参数

| 参数           | 类型                                                         | 默认值  | 描述                         |
| -------------- | ------------------------------------------------------------ | ------- | ---------------------------- |
| $queueName     | string                                                       |         | 队列名                       |
| $tag           | string                                                       |         | 消费者标签（区分不同消费者） |
| $noAck         | bool                                                         |         | 是否自动进行ACK              |
| $callback      | callable                                                     |         | 消息回调处理                 |
| $arguments     | [array](http://php-amqplib.github.io/php-amqplib/classes/PhpAmqpLib-Wire-AMQPTable.html) | array() | 参数                         |
| $prefetchSize  | int                                                          | null    | 消费者限流大小               |
| $prefetchCount | int                                                          | 1       | 消费者限流数量               |
| $nonBlocking   | bool                                                         | false   | 是否是阻塞的                 |
| $timeOut       | int                                                          | 0       | 超时时间                     |

##### send

发送MQ消息（事务）

```php
// @throws UtilException
$MQ->send( string $message, string $chanName, array $params, string $messageId = "auto" )
```

###### 参数

| 参数 | 必填 | 类型  | 描述 |
| ------- | ----- | ----- | ---- |
| $message | ✅ | array | 参数 |
| $chanName | ✅ | array | 参数 |
| $messageId |  | array | 参数 |

| 参数内容  | 类型   | 描述   |
| --------- | ------ | ------ |
| message   | string | 消息   |
| chanName  | string | 队列名 |
| messageId | string |        |

##### listen

监听队列消息封装

```php
// @throws UtilException
$mq->listen( [ 'chanName' => "Erp_ImportGoods" ], function ( AMQPMessage $msg ) {
  $body = $msg->getBody();
  var_dump($body);
  $msg->getChannel()->basic_ack( $msg->getDeliveryTag() );
});
```

###### 参数

| 参数      | 类型     | 描述                |
| --------- | -------- | ------------------- |
| $params   | array    | 参数 chanName  args |
| $callback | function | amqp消息回调        |

##### close

关闭MQConn 关闭MQChan

```php
// @throws UtilException
$mq->close();
```

#### MYSQLPDO

参考文档  [medoo](https://medoo.lvtao.net/1.2/doc.php)

继承 simpleHandle\Component\UtilPdo\BasePdo 类

```php
use simpleHandle\Component\UtilPdo\BasePdo;
class DB extends BasePdo
{
	/**
	 * @throws UtilException
	 */
	public function __construct() {
		$dbConfig = [
      'host'       => '127.0.0.1',
      'port'       => 3306,
      'username'   => 'root',
      'password'   => 'root',
      'database'   => 'databasename',
      'charset'    => 'utf8mb4',
      'unixSocket' => null,
      'options'    => [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
      ],
      'size'       => 64
    ];
		parent::__construct( $dbConfig );
	}
}
$db = new DB();
```

##### 配置

| 配置项     | 必填 | 类型   | 配置值                                                       |
| ---------- | ---- | ------ | ------------------------------------------------------------ |
| host       | ✅    | string | 127.0.0.1                                                    |
| port       | ✅    | int    | 3306                                                         |
| username   | ✅    | string | root                                                         |
| password   | ✅    | string | root                                                         |
| database   | ✅    | string | databasename                                                 |
| charset    | ✅    | string | utf8mb4                                                      |
| unixSocket |      | Null   | 空即可                                                       |
| options    |      | array  | [用于链接](http://www.php.net/manual/en/pdo.setattribute.php) |
| size       | ✅    | int    | 64                                                           |

#### Redis

参考文档  [swoole redis 客户端](https://wiki.swoole.com/#/coroutine_client/redis)

继承 simpleHandle\Component\UtilRedis\BaseRedis 类

```php
use simpleHandle\Component\UtilRedis\BaseRedis;
class REDIS extends BaseRedis
{
	/**
	 * @throws UtilException
	 */
	public function __construct() {
		$redisConfig = [
      'host'     => '47.100.226.141',       // Redis服务器
      'port'     => 58878,                   // Redis端口
      'auth'     => "Zn4419268.",           // Redis密码
      'db_index' => 1,                      // Redis库序号
      'timeout'  => 3.0,                    // 连接超时
      'size'     => 10,                     // 客户端异常重连次数
    ];
		parent::__construct( $redisConfig );
	}
}
$db = new DB();
```

##### 配置

| 配置项   | 必填 | 类型   | 配置值      |
| -------- | ---- | ------ | ----------- |
| host     | ✅    | string | 127.0.0.1   |
| port     | ✅    | int    | 3306        |
| auth     | ✅    | string | requirepass |
| db_index | ✅    | string | 1           |
| timeout  | ✅    | string | 3.0         |
| size     | ✅    | string | 10          |

#### Log

继承 simpleHandle\Component\UtilLog\Logger 类

```php
use simpleHandle\Component\UtilLog\Logger;
class Log extends Logger
{
	/**
	 * @throws Exception
	 * @throws UtilException
	 */
	public function __construct( string $fileName, bool $output = false ) {
		$loggerPath = ROOT . DIRECTORY_SEPARATOR . 'Log';
		$fileLock   = ROOT . DIRECTORY_SEPARATOR . 'composer.json';
		parent::__construct( $fileName, $loggerPath, $output, $fileLock );
	}
}
$log = new Log("logfile");
```

##### 配置

| 配置项      | 必填 | 类型   | 描述                                                         |
| ----------- | ---- | ------ | ------------------------------------------------------------ |
| $fileName   | ✅    | string | 文件名                                                       |
| $loggerPath | ✅    | string | 日志文件根路径                                               |
| $output     | ✅    | bool   | 时候输出到控制台                                             |
| $fileLock   |      | string | 文件锁（文件路径） 【并发会导致创建文件异常报错导致日志记录失败，如有多个进程写日志需设定该参数】 |

##### 示例

```php
$log->appendLog("hello"); // 拼接日志
$log->appendLog("world"); // 拼接日志
$log->write();// 日志写入到文件
/*
【2022-01-17 14:32:52】【INFO】hello
【2022-01-17 14:32:52】【INFO】world
*/

$log->appendLog("hello"); // 拼接日志
$log->writeLog("world");// 日志写入到文件
/*
【2022-01-17 14:32:52】【INFO】hello
【2022-01-17 14:32:52】【INFO】world
*/

$log->writeLine("hello",Logger::INFO);
// 【2022-01-17 14:32:52】【INFO】hello

/* 
Logger::SUCCESS
Logger::INFO
Logger::WARNING
Logger::ERROR
Logger::CRITICAL
Logger::DEBUG
*/
$log->success("hello");
$log->info("hello");
$log->warning("hello");
$log->error("hello");
$log->critical("hello");
$log->debug("hello");
```

#### Xls

参考文档  [Xlswirte](https://xlswriter-docs.viest.me/)

参考代码注释

#### Tool

参考代码注释

### 参与贡献

1. `php-amqplib/php-amqplib`
2. `godruoyi/php-snowflake`
3. `inhere/php-validate`
4. `ext-redis`
5. `ext-pdo`
6. `ext-json`
7. `ext-xlswriter`
8. `voku/anti-xss`
